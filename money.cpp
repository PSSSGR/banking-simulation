/*
 * Filename:	Money.cpp 
 * Name:		Sri Padala
 * Description:	This program takes input from user and builds up the statistics of their hotdog stands. 
 */
#include "money.hpp"
#include <iostream>
#include <cmath>
namespace  MyAwesomeBusiness{
Money::Money() // A default constructor that initializes your Money object to $0.00.
	:dollars(0), cents(0) {
}

Money::Money(int dollars, int cents) //A constructor that takes two integers, the ﬁrst for the dollars and the second for the cents .
	:dollars(dollars), cents(cents) {
}

Money::Money(int dollars)// A constructor that takes one integer, for an clean dollar amount .
	:dollars(dollars), cents(0){
}

Money::Money(double amount)//A constructor that takes one double, and can assign dollars and cents accordingly .
	: dollars(trunc(amount)), cents((round((amount-trunc(amount))*100))){
}
 
int Money::getPennies() const {//Returns the equivalent amount of money as a number of pennies 
	int totalPennies = (dollars) * 100 + (cents);
	return totalPennies;
}
 
bool Money::isNegative() const {
	if (dollars < 0 || cents < 0)//Checks if your amount of money is negative .
		return true;
	else
		return false;
}
 
void Money::add(const Money &m) {// The sum shall be stored in the calling object 
	int sum;
	sum = getPennies() + m.getPennies();
	int tempD, tempC;
	tempD = (sum) / 100;//Dollar part
	tempC = (sum) % 100;//Cent part 
	dollars = tempD;
	cents = tempC;
}
 
void Money::subtract(const Money &m) {
	int diff;
	diff = getPennies() - m.getPennies();
	int tempD, tempC;
	tempD = (diff) / 100;//Dollar part
	tempC = (diff) % 100;//Cent part
	dollars = tempD;
	cents = tempC;
}
 
bool Money::isEqual(const Money &m) const {
	if ((dollars == m.dollars) && (cents == m.cents)){
		return true;
	}else{
		return false;
	}
}
 
void Money::print() const {
	if (isNegative() == true) {//Print the negative amount.
		std::cout << "($" << std::abs(dollars) << ".";
		if ((cents < 10 && cents >= 0) || (cents > -10 && cents <= 0)){
			std::cout << "0";
		}
		std::cout << std::abs(cents) << ")";
	}else{
		std::cout << "$" << dollars << ".";
		if (cents < 10){
			std::cout << "0";
		}
		std::cout << cents;
	}
}
}
