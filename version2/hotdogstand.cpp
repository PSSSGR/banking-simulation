/*
 * Filename:	hotdogStand.cpp 
 * Name:		Sri Padala
 * Description:	This program takes input from command line and builds up the statistics of their hotdog stands. 
 */
#include <vector>
#include "hotdogstand.hpp"
#include "money.hpp"

	
namespace  MyAwesomeBusiness{
	int MyAwesomeBusiness::HotdogStand::HotdogStand_objects=0;//Static members of the hotdogstand class getting initialised.
	int MyAwesomeBusiness::HotdogStand::hotdogs_sold=0;
	Money MyAwesomeBusiness::HotdogStand::total_revenue_hotdogstands=Money(0);
    Money MyAwesomeBusiness::HotdogStand::total_remaining_cash=Money(0);
    HotdogStand::HotdogStand()
    :stand_money(0) ,hotdog_price(3.50) ,standSales_singleday(0),standSales(0),hourly_wage(7.25),hotdog_supply(60),max_hotdogs_allowed(60),Wholesale_hotdog_cost(0.15)
    {HotdogStand_objects++;};
    HotdogStand::HotdogStand(double price) // A constructor that takes a double that represents the price of a hotdog
    :stand_money(0) ,standSales_singleday(0),standSales(0)
    {
        if(price>3.50){
            hotdog_price=(price);
            hourly_wage=Money(8.00);
            hotdog_supply=30;
            max_hotdogs_allowed=30;
            Wholesale_hotdog_cost=Money(0.50);
        }else{
            hotdog_price=(price);
            hourly_wage=Money(7.25);
            hotdog_supply=60;
            max_hotdogs_allowed=60;
            Wholesale_hotdog_cost=Money(0.15);
        }
        HotdogStand_objects++;
    }//incrementing the hotdog stands.
    HotdogStand::HotdogStand(Money price)// A constructor that takes a Money object that represents the price of a hotdog.
        :stand_money(0) ,standSales_singleday(0),standSales(0)
    {
        double money=price.getPennies();

        if(money>350){
            hotdog_price=(price);
            hourly_wage=Money(8.00);
            hotdog_supply=30;
            max_hotdogs_allowed=30;
            Wholesale_hotdog_cost=Money(0.50);
        }else{
            hotdog_price=(price);
            hourly_wage=Money(7.25);
            hotdog_supply=60;
            max_hotdogs_allowed=60;
            Wholesale_hotdog_cost=Money(0.15);
        }
        HotdogStand_objects++;

    }
    const Money HotdogStand::getCash() const{//This returns the total cash the HotdogStand has made.
        Money temp;
		double temp1=hotdog_price.getPennies();	
		double price=static_cast<double>(temp1/100);
        temp = Money((price)*(standSales));
        total_revenue_hotdogstands=total_revenue_hotdogstands+temp;
        return(temp);
        }
    const Money HotdogStand::getPrice() const{return((hotdog_price));}
    int HotdogStand::getDailyDogsSold() const{return(standSales_singleday);}//This returns the number of hotdogs sold by the stand in one day 
    void HotdogStand::replenishSupplies(){// To buy hotdogs so that you have your maximum supply 
        if(hotdog_supply<max_hotdogs_allowed){
            Money temp;
		    double temp1=hotdog_price.getPennies();	
		    double price=static_cast<double>(temp1/100);
            temp = Money((price)*(standSales));
            int supply_needed=max_hotdogs_allowed-hotdog_supply;
            Money money_needed=(supply_needed)*(Wholesale_hotdog_cost);
            stand_money=stand_money+temp-money_needed;
            total_remaining_cash=total_remaining_cash+temp-money_needed;
            hotdog_supply=max_hotdogs_allowed;
        } 
    }
    void HotdogStand::payWorker(){ // The wages of HotdogStand employee are set aside from your cash daily 
        Money work;
        work=(hours_worked)*(hourly_wage);
        stand_money=(stand_money)-(work);
        total_remaining_cash=total_remaining_cash-(work);
    }
    void HotdogStand::dailyReset(){// Resets the appropriate private data members in order to correctly simulate another day 
        standSales_singleday=0;
        standSales=0;
     }
    const Money HotdogStand::getStandRevenue() const {//This calculates the total money made by selling hotdogs 
        return(stand_money);
    }
    void HotdogStand::setPrice(double price){hotdog_price=price;}
    void HotdogStand::sellHotdog(){standSales_singleday++;standSales++;hotdogs_sold++;hotdog_supply--;}//Increments the private members accordingly.
	int HotdogStand::getNumStands(){return(HotdogStand_objects);}
    int HotdogStand::getTotalHotdogsSold(){return(hotdogs_sold);}
    const Money HotdogStand::getTotalRevenue(){return(total_revenue_hotdogstands);} 
    const Money HotdogStand::getTotalmoney(){return(total_remaining_cash);} 
}
