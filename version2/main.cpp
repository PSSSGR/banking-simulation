

/*
 * Filename:	main.hpp
 * Name:		Sri Padala
 * Description:	This program takes input from command line and builds up the statistics of their hotdog stands.
 */
#include <iostream>
#include <iomanip>
#include <string>
#include <cstring>
#include <vector>
#include "hotdogstand.hpp"
#include "BusinessFunctions.hpp"



int main(int argc, char *argv[])
{
	namespace MAB = MyAwesomeBusiness;

	std::vector<MAB::HotdogStand> franchises;

	int stands;
	int fancyStands;
	MAB::Money fancyPrice;
	int days;

	do {
		 days=atoi(argv[1]);
		if(days <=0){
			std::cout<<"Please enter a positive number of days.\n";
			exit(1);
		}
	} while(days <= 0);

	do {
		stands=atoi(argv[2]);
		if(stands<0){
			std::cout<<"Please enter a positive number of stands.\n";
			exit(1);
		}
	} while(stands < 0);

	if(!(argv[3])){
		fancyStands=0;
	}else{
		if(atoi(argv[3])<0){
			std::cout<<"Please enter a positive or zero number of fancy hotdog stands.\n";
			exit(1);
		}else{
		fancyStands=atoi(argv[3]);
		}
	}

	if (fancyStands <= stands && fancyStands > 0) {
		std::string money;
		double price;
		MAB::Money right;
		if(strcmp( argv[4], "-c") == 0 ||strcmp( argv[4], "-cost") == 0 ){
			money=argv[5];
			price=std::stod(money);
			right=MAB::Money(price);
			fancyPrice=right;
		}else{
			std::cout<<"error";
			exit(1);
		}
	}

	// Create vector
	for (int i = 0; i < stands; i++) {
		if (i < stands - fancyStands) {
			franchises.emplace_back();
		} else {
			franchises.emplace_back(fancyPrice);
		}
	}

	MAB::runSimulation(franchises, days);//Calling the nonmember function.

	return 0;
}
