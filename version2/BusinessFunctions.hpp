/*
 * FILENAME:	BusinessFunctions.hpp 
 * Name:		Sri Padala
 * Description:	This program takes input from command line and builds up the statistics of their hotdog stands. 
 */
#ifndef BUSINESSFUNCTIONS_HPP
#define BUSINESSFUNCTIONS_HPP
#include "money.hpp"
#include "hotdogstand.hpp"
#include <vector>

namespace  MyAwesomeBusiness{
    void runSimulation(std::vector<HotdogStand> &franchises, int days);//This function runs the simulation for an end-user speciﬁed number of days.
    void printRundown(const std::vector<HotdogStand> &franchises);// This function will print the summary of the final statistics of the franchises.
}
#endif
