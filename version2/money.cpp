/*
 * Filename:	money.cpp 
 * Name:		Sri Padala
 * Description:	This program takes input from command line and builds up the statistics of their hotdog stands. 
 */
#include "money.hpp"
#include <iostream>
#include <string>
#include <cmath>

namespace  MyAwesomeBusiness{
Money::Money() // A default constructor that initializes your Money object to $0.00.
	:dollars(0), cents(0) {
}

Money::Money(int dollars, int cents) //A constructor that takes two integers, the ﬁrst for the dollars and the second for the cents .
	:dollars(dollars), cents(cents) {
}

Money::Money(int dollars)// A constructor that takes one integer, for an clean dollar amount .
	:dollars(dollars), cents(0){
}

Money::Money(double amount)//A constructor that takes one double, and can assign dollars and cents accordingly .
	: dollars(trunc(amount)), cents((round((amount-trunc(amount))*100))){
}
 
int Money::getPennies() const {//Returns the equivalent amount of money as a number of pennies 
	int totalPennies = (dollars) * 100 + (cents);
	return totalPennies;
}
 
bool Money::isNegative() const {
	if (dollars < 0 || cents < 0)//Checks if your amount of money is negative .
		return true;
	else
		return false;
}
 
const Money Money::operator -() const
{
    return (Money(-dollars,-cents));//negates both the dollar and cent part.
}
Money& Money::operator ++()
{
    ++dollars;
    return *this;
}
const Money Money::operator++(int)
{
    Money temp = *this;
    ++(*this);

    return temp;
}
Money& Money::operator --()
{
    --dollars;
    return *this;
}

const Money Money::operator --(int)
{
    Money temp = *this;
    --(*this);
    return temp;
}
const Money operator +(const Money& left, const Money& right)
{
    Money sum;
    sum.dollars = left.dollars + right.dollars;
    sum.cents = left.cents + right.cents;
    if(sum.cents >= 100){//if the cents cross 100 , it will add 1 to the dollar part.
        sum.dollars+=1;
        sum.cents=sum.cents-100;
    }
    return sum;
}
const Money operator *(const Money& left, double right)
{
    double prodDollars=left.dollars * right;
    double prodCents = left.cents * right;
    double totalCents = prodCents + 100 * prodDollars;
    double totalDollars = totalCents/100;

    int dollarResult = std::floor(totalDollars);
    double dollars1 = totalDollars - dollarResult;
    int centsResult = std::round(dollars1*100);

    Money product(dollarResult,centsResult); 
    return(product); 
}
const Money operator *(double left, const Money& right){
    return(right*left);//calls the main product operator.
}
const Money operator *(const Money& left, int right){
    return(left*(static_cast<double>(right)));//calls the main product operator.
}
const Money operator *(int left, const Money& right){
    return(right *(static_cast<double>(left)));//calls the main product operator.
}
bool operator >(const Money& left, const Money& right){
    return (left.getPennies() > right.getPennies());
}
bool operator >=(const Money& left, const Money& right){
    return (left.getPennies() >= right.getPennies());
}
bool operator <(const Money& left, const Money& right){
    return (left.getPennies() < right.getPennies());
}
bool operator <=(const Money& left, const Money& right){
    return (left.getPennies() <= right.getPennies());
}
bool operator ==(const Money& left, const Money& right){
    return (left.getPennies() == right.getPennies());
}
bool operator !=(const Money& left, const Money& right){
    return (left.getPennies() != right.getPennies());
}
std::ostream& operator <<(std::ostream& outStream, const Money& right)
{
int money=right.getPennies();
int dollars= money/100;
int cents=money%100;

std::string cash;
    	if (right.isNegative() == true) {//Print the negative amount.
            cash="($"+std::to_string(std::abs(dollars))+".";
		if ((cents < 10 && cents >= 0) || (cents > -10 && cents <= 0)){
            cash=cash+"0";
		}
        cash=cash+std::to_string(std::abs(cents))+")";
        outStream<<cash;
	}else{
        cash="$"+std::to_string(std::abs(dollars))+".";
		if (cents < 10){
            cash=cash+"0";
		}
        cash=cash+std::to_string(std::abs(cents));
        outStream<<cash;
	}

	return(outStream);//returns the output stream.
}
std::istream& operator >>(std::istream& inStream, Money& right)
{
    std::string money;
    double price;
    inStream>>money;
    if(money[0]=='$'){
			money=money.erase(0,1);//erases the $ sign.
			price = std::stod(money);
		} else {
			price = std::stod(money);
		}
	right=Money(price);
    return(inStream);//returns the input stream.
}
Money operator -(const Money& left, const Money& right){
    int diff;
	diff = left.getPennies() - right.getPennies();
	int tempD, tempC;
	tempD = (diff) / 100;//Dollar part
	tempC = (diff) % 100;//Cent part
    return(Money(tempD,tempC));
}
}
