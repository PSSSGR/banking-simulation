/*
 * Filename:	BusinessFunctions.cpp 
 * Name:		Sri Padala
 * Description:	This program takes input from command line and builds up the statistics of their hotdog stands. 
 */

#include <iostream>
#include <random>
#include <vector>
#include <fstream>
#include <iomanip>
#include "hotdogstand.hpp"
#include "money.hpp"
#include "BusinessFunctions.hpp"

std::ofstream out_stream("sim.txt");//opening the file .


namespace  MyAwesomeBusiness{
	int numdays=0;
    void runSimulation(std::vector<HotdogStand> &franchises, int days){
		numdays=days;
        for(int i=0;i<days;i++){
            for(unsigned int z =0;z<franchises.size();z++){
				franchises[z].dailyReset();
				if((franchises[z].getPrice().getPennies())<=350){
			    	std::random_device rd;
			    	std::mt19937 generator(rd());
			    	std::uniform_int_distribution<int> uniDist(20, 60);//Gives random number in the given range.
			    	int sales = uniDist(generator);
			    	for (auto s = 1; s <= sales; ++s){
                     	franchises[z].sellHotdog(); //Increments all appropriate data members accordingly 
                 	}
			    }else{
			    	std::random_device rd;
			    	std::mt19937 generator(rd());
			    	std::uniform_int_distribution<int> uniDist(1, 30);//Gives random number in the given range.
			    	int sales = uniDist(generator);
			    	for (auto j = 1; j <= sales; ++j){
			    	    franchises[z].sellHotdog();// Increments all appropriate data members accordingly 
			    	}
			    }
				franchises[z].replenishSupplies(); 
				franchises[z].payWorker();	
	    	} 
			printRundown(franchises);	

        }
    }
    void printRundown(const std::vector<HotdogStand> &franchises){
		static int counter=0;
		counter++;
        out_stream << "\nStand	Sales	Price	Revenue Remaining Cash\n";
        out_stream<<"=====	=====	=====	======= ==============\n";
        for(unsigned int i =0; i<franchises.size(); i++){
		    Money temp=franchises[i].MyAwesomeBusiness::HotdogStand::getPrice();
          if(temp.getPennies()==350){//Checks for the general price.
		    	int sales=franchises[i].getDailyDogsSold();
		    	out_stream << std::setw(5) << std::right << std::setfill(' ') <<i+1<< std::setw(8) << std::right<< std::setfill(' ')<<sales;
		    	out_stream << std::setw(8) << std::right << std::setfill(' ');
		    	out_stream<<temp;//Prints the price of the hotdog.
                out_stream<<std::setw(10) << std::right << std::setfill(' ');
		    	Money temp3=franchises[i].getCash();
                out_stream<<temp3;//Prints the stand revenue.
				out_stream<<std::setw(15) << std::right << std::setfill(' ');
		    	Money tempa=franchises[i].getStandRevenue();
                out_stream<<tempa;//Prints the stand revenue.
                out_stream<<"\n";
            }else{
                int sales=franchises[i].MyAwesomeBusiness::HotdogStand::getDailyDogsSold();
                out_stream<< std::setw(5) << std::right << std::setfill(' ') <<i+1<< std::setw(8) << std::right<<std::setfill(' ') <<sales;
			    Money temp4=franchises[i].getPrice();
			    out_stream << std::setw(8) << std::right << std::setfill(' ');
                out_stream<<temp4;//Prints the price of the hotdog.
			    out_stream<<std::setw(10) << std::right << std::setfill(' ');
			    Money temp5=franchises[i].getCash();
                out_stream<<temp5;//Prints the stand revenue.
				out_stream<<std::setw(15) << std::right << std::setfill(' ');
		    	Money tempb=franchises[i].getStandRevenue();
                out_stream<<tempb;//Prints the stand revenue.
            out_stream<<"\n";
            }
			
        }
		if(counter==numdays){
        		Money temp6;
        		temp6=MyAwesomeBusiness::HotdogStand::getTotalRevenue();
        		out_stream<<"\nTotals    ";
        		out_stream<<std::setw(3) << std::right << std::setfill(' ')<<MyAwesomeBusiness::HotdogStand::getTotalHotdogsSold();
    			out_stream << std::setw(18) << std::right << std::setfill(' ');
      		  	out_stream<<temp6;//Prints the total revenue of the hotdog business.
				out_stream << std::setw(15) << std::right << std::setfill(' ');
      		  	out_stream<<MyAwesomeBusiness::HotdogStand::getTotalmoney();	
    			out_stream<<"\n";
     			out_stream<<"# of Stands: ";
     			out_stream<<MyAwesomeBusiness::HotdogStand::getNumStands()<<std::endl;
		}

    }
}