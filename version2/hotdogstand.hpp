/*
 * Filename:	hotdogStand.hpp 
 * Name:		Sri Padala
 * Description:	This program takes input from command line and builds up the statistics of their hotdog stands. 
 */
#ifndef HOTDOGSTAND_HPP
#define HOTDOGSTAND_HPP
#include"money.hpp"

namespace  MyAwesomeBusiness{
    class HotdogStand {
        public:
            HotdogStand();//Constructors
            HotdogStand(double price);
            HotdogStand(Money price);
            const Money getCash() const;// This returns the total cash the HotdogStand has. 
            const Money getPrice() const;//This returns the pice of the hotdog.
            int getDailyDogsSold() const;//This returns the number of hotdogs sold by the stand. 
            void replenishSupplies();
            void payWorker();
            void dailyReset();
            const Money getStandRevenue() const;//This calculates the total money made by selling hotdogs .
            void setPrice(double price);
            void sellHotdog();//Increments all appropriate data members accordingly .
            static int getNumStands();//Returns the number of stands.
            static int getTotalHotdogsSold();//Returns the total hotdogs sold. 
            static const Money getTotalRevenue();//Returns the total revenue money object.
            static const Money getTotalmoney();//Returns the total remaining money object
    
    
        private:
            Money stand_money;//A Money object that will represent how much money the stand has made .
            Money hotdog_price;// A Money object that will represent the price of a hotdog .
            int standSales_singleday;//An integer that will describe how many hotdogs a stand has sold in one day.
            int standSales;
            Money hourly_wage;
            int hours_worked=8;
            int hotdog_supply;
            int max_hotdogs_allowed;
            Money Wholesale_hotdog_cost;
            static int  HotdogStand_objects;//A static integer that represents the total number of HotdogStand objects.
            static int  hotdogs_sold;//A static integer that represents the total number of hotdogs sold.
            static Money total_revenue_hotdogstands;//A static Money object that represents total revenue for all HotdogStand objects.
            static Money total_remaining_cash;//Makes count of the total remaining cash.
    };
	
}
#endif