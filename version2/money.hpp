/*
 * Filename:	money.hpp
 * Name:		Sri Padala
 * Description:	This program takes input from command line and builds up the statistics of their hotdog stands. 
 */
#ifndef MONEY_HPP
#define MONEY_HPP
#include <iostream>


namespace  MyAwesomeBusiness{
    class Money{
        public:
            Money();//Constructors
		    Money(int dollars,int cents);
		    Money(int amount);
		    Money(double amount);
		    int getPennies() const;// Returns the equivalent amount of money as a number of pennies .
		    bool isNegative() const;//Returns true if your amount of money is negative .
		  
			const Money operator -() const;//Unary negation operator.
			Money& operator ++();// Preﬁx&Postﬁx increment (Increment dollars by one) 
    		const Money operator++(int);
    		Money& operator --();//Preﬁx&Postﬁx decrement (Decrement dollars by one) 
    		const Money operator --(int);
			friend const Money operator +(const Money& left, const Money& right);// Addition of two Money objects 
			friend const Money operator *(const Money& left, int right);
    		friend const Money operator *(int left, const Money& right);
			friend const Money operator *(const Money& left, double right);
    		friend const Money operator *(double left, const Money& right);
			friend bool operator >(const Money& left, const Money& right);//Comparison operators.
    		friend bool operator >=(const Money& left, const Money& right);
    		friend bool operator <(const Money& left, const Money& right);
			friend bool operator <=(const Money& left, const Money& right);
    		friend bool operator ==(const Money& left, const Money& right);
    		friend bool operator !=(const Money& left, const Money& right);
			friend std::ostream& operator <<(std::ostream& outStream, const Money& right);
    		friend std::istream& operator >>(std::istream& inStream, Money& right);

	    private:
	    	int dollars;
	    	int cents;
    };
	Money operator -(const Money& left, const Money& right);		
}
#endif