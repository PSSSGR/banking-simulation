
 # Filename:	makefile 
 # Name:		Sri Padala
 
 # Description:	This program takes input from user and builds up the statistics of their hotdog stands. 

hw03:main.cpp money.o hotdog.o nonmember.o#compiles the main.cpp and links all object files.
	g++ -Wall -g -std=c++11 -o hw03 main.cpp money.o hotdog.o nonmember.o 
money.o:money.cpp money.hpp#creates the object file for the money file and compiles it but does not link it.
	g++ -Wall -g -c -std=c++11 money.cpp
hotdog.o:hotdog.cpp hotdog.hpp#creates the object file for the hotdog file and compiles it but does not link it.
	g++ -Wall -g -c -std=c++11 hotdog.cpp
nonmember.o:nonmember.cpp nonmember.hpp#creates the object file for the nonmember file and compiles it but does not link it.
	g++ -Wall -g -c -std=c++11 nonmember.cpp
clean:#removes the object and executable file.
	rm -f *.o hw03
