/*
 * Filename:	nonmember.hpp 
 * Name:		Sri Padala
 * Description:	This program takes input from user and builds up the statistics of their hotdog stands. 
 */
#ifndef NONMEMBER_HPP
#define NONMEMBER_HPP
#include "money.hpp"
#include "hotdog.hpp"
#include <vector>

namespace  MyAwesomeBusiness{
    void printRundown(const std::vector<HotdogStand> &franchises, int num);// This function will print the summary of the final statistics of the franchises.
}
#endif
