/*
 * Filename:	nonmember.cpp 
 * Name:		Sri Padala
 * Description:	This program takes input from user and builds up the statistics of their hotdog stands. 
 */
#include<iostream>
#include<random>
#include<vector>
#include<iomanip>
#include"hotdog.hpp"
#include"money.hpp"
#include"nonmember.hpp"
namespace  MyAwesomeBusiness{
    void printRundown(const std::vector<HotdogStand> &franchises, int num){
        std::cout<<"\nStand	Sales	Price	Revenue \n";
        std::cout<<"=====	=====	=====	======= \n";
        for(int i =0; i<num; i++){
		    Money temp=franchises[i].MyAwesomeBusiness::HotdogStand::getPrice();
          if(temp.getPennies()==350){//Checks for the general price.
		    	int sales=franchises[i].MyAwesomeBusiness::HotdogStand::getDogsSold();
		    	std::cout << std::setw(5) << std::right << std::setfill(' ') <<i+1<< std::setw(8) << std::right<< std::setfill(' ')<<sales;
		    	Money temp2=franchises[i].getPrice();
		    	std::cout << std::setw(4) << std::right << std::setfill(' ');
		    	temp2.print();//Prints the price of the hotdog.
                std::cout<<std::setw(4) << std::right << std::setfill(' ');
		    	Money temp3=franchises[i].getStandRevenue();
                temp3.print();//Prints the stand revenue.
                std::cout<<"\n";
            }else{
                int sales=franchises[i].MyAwesomeBusiness::HotdogStand::getDogsSold();
                std::cout<< std::setw(5) << std::right << std::setfill(' ') <<i+1<< std::setw(8) << std::right<<std::setfill(' ') <<sales;
			    Money temp4=franchises[i].getPrice();
			    std::cout << std::setw(4) << std::right << std::setfill(' ');
                temp4.print();//Prints the price of the hotdog.
			    std::cout<<std::setw(4) << std::right << std::setfill(' ');
			    Money temp5=franchises[i].getStandRevenue();
                temp5.print();//Prints the stand revenue.
                std::cout<<"\n";
            }

        }
        Money temp6;
        temp6=MyAwesomeBusiness::HotdogStand::getTotalRevenue();
        std::cout<<"\nTotals    ";
        std::cout<<std::setw(3) << std::right << std::setfill(' ')<<MyAwesomeBusiness::HotdogStand::getTotalHotdogsSold();
    	std::cout << std::setw(12) << std::right << std::setfill(' ');
        temp6.print();//Prints the total revenue of the hotdog business.
    	std::cout<<"\n";
        std::cout<<"# of Stands: ";
        std::cout<<MyAwesomeBusiness::HotdogStand::getNumStands()<<std::endl;


    }
}
