/*
 * Filename:	Money.hpp
 * Name:		Sri Padala
 * Description:	This program takes input from user and builds up the statistics of their hotdog stands. 
 */
#ifndef MONEY_HPP
#define MONEY_HPP


namespace  MyAwesomeBusiness{
    class Money{
        public:
            Money();//Constructors
		    Money(int dollars,int cents);
		    Money(int amount);
		    Money(double amount);
		    int getPennies() const;// Returns the equivalent amount of money as a number of pennies .
		    bool isNegative() const;//Returns true if your amount of money is negative .
		    void add(const Money &m);//The sum shall be stored in the calling object .
		    void subtract(const Money &m);//The difference shall be stored in the calling object .
		    bool isEqual(const Money &m) const;//Returns true if the comparison is equal .
		    void print() const ;//Prints the money object.
	    private:
	    	int dollars;
	    	int cents;
    };
}
#endif
