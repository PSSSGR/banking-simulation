/*
 * Filename:	HotdogStand.cpp 
 * Name:		Sri Padala
 * Description:	This program takes input from user and builds up the statistics of their hotdog stands. 
 */

#include "hotdog.hpp"
#include "money.hpp"

	
namespace  MyAwesomeBusiness{
	int MyAwesomeBusiness::HotdogStand::HotdogStand_objects=0;
	int MyAwesomeBusiness::HotdogStand::hotdogs_sold=0;
	Money MyAwesomeBusiness::HotdogStand::total_revenue_hotdogstands=Money(0);
    HotdogStand::HotdogStand() :stand_money(0) ,hotdog_price(3.50) ,standSales(0) {HotdogStand_objects++;};
    HotdogStand::HotdogStand(double price) :stand_money(0) ,hotdog_price(price) ,standSales(0) {HotdogStand_objects++;};//incrementing the hotdog stands.
    const Money HotdogStand::getCash() const{return(Money(stand_money));}
    const Money HotdogStand::getPrice() const{return(Money(hotdog_price));}
    int HotdogStand::getDogsSold() const{return(standSales);}
    const Money HotdogStand::getStandRevenue() const {
        Money temp;
		double temp1=hotdog_price.getPennies();	
		double price=static_cast<double>(temp1/100);
        temp = Money((price)*(standSales));
		total_revenue_hotdogstands.add(temp);//Adds to the total revenue.
        return(temp);
    }
    void HotdogStand::setPrice(double price){hotdog_price=price;}
    void HotdogStand::sellHotdog(){standSales++;hotdogs_sold++;}//Increments the private members accordingly.
	int HotdogStand::getNumStands(){return(HotdogStand_objects);}
    int HotdogStand::getTotalHotdogsSold(){return(hotdogs_sold);}
    const Money HotdogStand::getTotalRevenue(){return(total_revenue_hotdogstands);} 
}
