/*
 * Filename:	main.cpp 
 * Name:		Sri Padala
 * Description:	This program takes input from user and builds up the statistics of their hotdog stands. 
 */

#include<iostream>
#include<vector>
#include<string>
#include<random>
#include"money.hpp"
#include"hotdog.hpp"
#include"nonmember.hpp"

int main(void)
{
    int stands,classy_hotdogs;
	double price;
	std::vector<MyAwesomeBusiness::HotdogStand> franchises;//vector of HotdogStand objects.
    std::cout<<"Welcome!\n"<<"How many hotdog stands do you own? ";
    std::cin>>stands;
    std::cout<<"\nHow many of those sell classy hotdogs? ";
    std::cin>>classy_hotdogs;
    if(classy_hotdogs>0){
		std::string hotdogs;
        std::cout<<"\nHow much does a classy hotdog cost? ";
        std::cin>>hotdogs;

		if(hotdogs[0]=='$'){
			hotdogs=hotdogs.erase(0,1);//erases the $ sign.
			price = std::stod(hotdogs);
		} else {
			price = std::stod(hotdogs);
		}
		for(int z =0;z<stands;z++){
			if(z<(stands-classy_hotdogs)){
				franchises.emplace_back();//Calling the default constructor.
				std::random_device rd;
				std::mt19937 generator(rd());
				std::uniform_int_distribution<int> uniDist(20, 60);//Gives random number in the given range.
				int sales = uniDist(generator);
				for (auto s = 1; s <= sales; ++s){
                	franchises[z].sellHotdog(); //Increments all appropriate data members accordingly 
            	}
			}else{
				franchises.emplace_back(MyAwesomeBusiness::HotdogStand(price));//Calling the constructor.
				std::random_device rd;
				std::mt19937 generator(rd());
				std::uniform_int_distribution<int> uniDist(1, 30);//Gives random number in the given range.
				int sales = uniDist(generator);
				for (auto j = 1; j <= sales; ++j){
				    franchises[z].sellHotdog();// Increments all appropriate data members accordingly 
				}
			}
		}
        printRundown(franchises, stands);//Calling the nonmember function.
	}else{
			std::cout<<"\nPlease enter a positive number\n";
	}
	

    return(0);
}
