/*
 * Filename:	HotdogStand.hpp 
 * Name:		Sri Padala
 * Description:	This program takes input from user and builds up the statistics of their hotdog stands. 
 */
#ifndef HOTDOG_HPP
#define HOTDOG_HPP
#include"money.hpp"

namespace  MyAwesomeBusiness{
    class HotdogStand {
        public:
            HotdogStand();//Constructors
            HotdogStand(double price);
            const Money getCash() const;// This returns the total cash the HotdogStand has. 
            const Money getPrice() const;//This returns the pice of the hotdog.
            int getDogsSold() const;//This returns the number of hotdogs sold by the stand. 
            const Money getStandRevenue() const;//This calculates the total money made by selling hotdogs .
            void setPrice(double price);
            void sellHotdog();//Increments all appropriate data members accordingly .
            static int getNumStands();//Returns the number of stands.
            static int getTotalHotdogsSold();//Returns the total hotdogs sold. 
            static const Money getTotalRevenue();//Returns the total revenue money object.
            int getSales() const ;
            static int getTotalSales();
    
    
        private:
            Money stand_money;//A Money object that will represent how much money the stand has made .
            Money hotdog_price;// A Money object that will represent the price of a hotdog .
            int standSales;//An integer that will describe how many hotdogs a stand has sold .
            static int  HotdogStand_objects ;//A static integer that represents the total number of HotdogStand objects.
            static int  hotdogs_sold;//A static integer that represents the total number of hotdogs sold.
            static Money total_revenue_hotdogstands;//A static Money object that represents total revenue for all HotdogStand objects.

    };
	
}
#endif
